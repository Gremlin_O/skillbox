export default {
	preset: 'ts-jest',
	testEnvironment: 'jsdom',
	moduleNameMapper: {
		'\\.(jpg|jpeg|png|svg)$': '<rootDir>/mocks/mock.js',
		'\\.(css|less)$': '<rootDir>/mocks/mock.js',
	},
};
