import { Link, useParams } from 'react-router-dom';
import { USERS } from '../../data';
import './UserInfoPage.css';
import { IUserInfoPageProps } from './interfaces';

export function UserInfoPage() {
	const { userId } = useParams();

	const user = USERS.find(({ id }) => id === Number(userId));

	if (!user) {
		return (
			<div className='userInfoPage'>
				<h2>UserInfoPage</h2>

				<div className='users'>
					<p data-testid='nouser-text'>Пользоатвеля с таким ИД нет</p>
				</div>
			</div>
		);
	}

	return (
		<div className='userInfoPage'>
			<h2>UserInfoPage</h2>

			<div className='users'>
				<p>{user.jobTitle}</p>
				<p data-testid='email'>{user.email}</p>
				<img src={user.avatar} alt='' width={200} height={200} />
				<p data-testid='name'>{user.fullName}</p>
				<p>{user.bio}</p>
				{user.playlist && (
					<Link to={`/playlists/${user.playlist.id}`}>{user.playlist.name}</Link>
				)}
			</div>
		</div>
	);
}
