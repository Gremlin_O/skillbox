import { render } from '@testing-library/react';
import * as userslistsObject from '../../../data/users';
import { IPlaylist, IUser } from '../../../data/interfaces';
import { UserInfoPage } from '../UserInfoPage';
import { LinkProps, useParams } from 'react-router-dom';

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useParams: jest.fn(),
	Link: (props: LinkProps) => <a data-testid={'playlist-link'}>{props.children}</a>,
}));

const mockUseParams = useParams as jest.Mock;

describe('Тестирование компонента UserInfoPage', () => {
	test('Проверка текста по умолчанию в случае отсутствия пользователя', () => {
		mockUseParams.mockImplementation(() => ({
			userId: 99999,
		}));
		(userslistsObject as any).USERS = [];
		const { getByTestId } = render(<UserInfoPage />);
		const pEl = getByTestId('nouser-text');
		expect(pEl).toBeDefined();
		expect(pEl.innerHTML).toBe('Пользоатвеля с таким ИД нет');
	});
	test('Проверка текста в случае присутствия пользователя с запрашиваемым id', () => {
		mockUseParams.mockImplementation(() => ({
			userId: 0,
		}));
		const testUser: IUser = {
			id: 0,
			avatar: '#',
			bio: '#',
			email: '@test.email',
			fullName: 'Gregory',
			jobTitle: 'Greg',
			playlist: {
				genre: 'test-genre',
				id: 0,
				name: 'test-name',
				tracks: [{ name: 'test-track1' }, { name: 'test-track2' }],
			},
		};
		(userslistsObject as any).USERS = [testUser];
		const { getByTestId } = render(<UserInfoPage />);
		const emailEl = getByTestId('email');
		expect(emailEl.innerHTML).toBe(testUser.email);
		const nameEl = getByTestId('name');
		expect(nameEl.innerHTML).toBe(testUser.fullName);
		const linkEl = getByTestId('playlist-link');
		expect(linkEl.innerHTML).toBe(testUser.playlist!.name);
	});
});
