import { Link, useSearchParams } from 'react-router-dom';
import { PLAYLISTS } from '../../data/playlists';
import './PlaylistsPage.css';
import { ChangeEvent } from 'react';

export const PlaylistsPage = () => {
	const [searchParams, setSearchParams] = useSearchParams();

	const handleSearchGenre = (event: ChangeEvent<HTMLInputElement>): void => {
		searchParams.set('genre', event.target.value.toLowerCase());
		setSearchParams(searchParams);
	};
	const handleSearchName = (event: ChangeEvent<HTMLInputElement>): void => {
		searchParams.set('name', event.target.value.toLowerCase());
		setSearchParams(searchParams);
	};

	const searchGenre = searchParams.get('genre') ?? '';
	const searchName = searchParams.get('name') ?? '';

	const filteredPlaylists = PLAYLISTS.filter(
		({ genre, name }) =>
			genre.toLowerCase().includes(searchGenre.toLowerCase()) &&
			genre !== 'Non Music' &&
			name.toLowerCase().includes(searchName.toLowerCase())
	);
	return (
		<div className='playlistsPage'>
			<h2>PlaylistsPage</h2>
			<div className='playlists'>
				<label>
					введите жанр{' '}
					<input
						data-testid='genre-input'
						type='text'
						value={searchGenre}
						onChange={handleSearchGenre}
					/>
				</label>
				<label>
					введите название{' '}
					<input
						data-testid='name-input'
						type='text'
						value={searchName}
						onChange={handleSearchName}
					/>
				</label>

				<div style={{ marginTop: 10 }}>
					{filteredPlaylists.map(({ id, name, genre }) => (
						<div key={id}>
							<Link to={`/playlists/${id}`} className='link'>
								<span>{name}</span>
								<span style={{ marginLeft: 10 }}>(Жанр {genre})</span>
							</Link>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};
