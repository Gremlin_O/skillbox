import { fireEvent, render } from '@testing-library/react';
import { LinkProps, useSearchParams } from 'react-router-dom';
import { PlaylistsPage } from '../PlaylistsPage';

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	Link: (props: LinkProps) => <a></a>,
	useSearchParams: jest.fn(),
}));

describe('Тестирование компонента PlaylistsPage', () => {
	test('setSearchParam вызывается при вводе жанра', () => {
		const spy = jest.fn();
		(useSearchParams as jest.Mock).mockImplementation(() => [{ get: () => '', set: () => {} }, spy]);
		const { getByTestId } = render(<PlaylistsPage />);
		const genreInput = getByTestId('genre-input');
		fireEvent.change(genreInput, { target: { value: 'test' } });
		expect(spy).toHaveBeenCalled();
	});
	test('setSearchParam вызывается при вводе названия', () => {
		const spy = jest.fn();
		(useSearchParams as jest.Mock).mockImplementation(() => [{ get: () => '', set: () => {} }, spy]);
		const { getByTestId } = render(<PlaylistsPage />);
		const nameInput = getByTestId('name-input');
		fireEvent.change(nameInput, { target: { value: 'test' } });
		expect(spy).toHaveBeenCalled();
	});
});
