import { useParams } from 'react-router-dom';
import './PlaylistInfoPage.css';
import { PLAYLISTS } from '../../data/playlists';

export const PlaylistInfoPage = () => {
	const { playlistId } = useParams();
	const playlist = PLAYLISTS.find(({ id }) => id === Number(playlistId));

	if (!playlist) {
		return (
			<div className='playlistInfoPage'>
				<h2>PlaylistInfoPage</h2>

				<div className='playlists'>
					<p data-testid='noplay-list'>Плейлиста с таким ИД нет</p>
				</div>
			</div>
		);
	}

	return (
		<div className='playlistInfoPage'>
			<h2>PlaylistInfoPage</h2>

			<div className='playlists'>
				<p>
					<span>Жанр:</span>
					<span style={{ fontWeight: 700, marginLeft: 10 }} data-testid={'genre'}>
						{playlist.genre}
					</span>
				</p>
				<p>
					<span>Название:</span>
					<span style={{ fontWeight: 700, marginLeft: 10 }} data-testid={'name'}>
						{playlist.name}
					</span>
				</p>
				<hr />
				<div data-testid={'tracks'}>
					{playlist.tracks.map((track) => (
						<p key={track.name}>- {track.name}</p>
					))}
				</div>
			</div>
		</div>
	);
};
