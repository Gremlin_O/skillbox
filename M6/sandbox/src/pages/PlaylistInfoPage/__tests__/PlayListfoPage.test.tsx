import { render } from '@testing-library/react';
import { PlaylistInfoPage } from '../PlaylistInfoPage';
import { useParams } from 'react-router-dom';
import * as playlistsObject from '../../../data/playlists';
import { IPlaylist } from '../../../data/interfaces';

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	useParams: jest.fn(),
}));

const mockUseParams = useParams as jest.Mock;

describe('Тестирование компонента PlaylistInfoPage', () => {
	test('Проверка текста по умолчанию в случае отсутствия доступного плейлиста', () => {
		mockUseParams.mockImplementationOnce(() => ({
			playlistId: 99999,
		}));
		(playlistsObject as any).PLAYLISTS = [];
		const { getByTestId } = render(<PlaylistInfoPage />);
		const pEl = getByTestId('noplay-list');
		expect(pEl).toBeDefined();
		expect(pEl.innerHTML).toBe('Плейлиста с таким ИД нет');
	});
	test('Проверка текста в случае присутствия плейлиста с запрашиваемым id', () => {
		mockUseParams.mockImplementationOnce(() => ({
			playlistId: 0,
		}));
		const testPlaylist = {
			genre: 'test-genre',
			id: 0,
			name: 'test-name',
			tracks: [{ name: 'test-track1' }, { name: 'test-track2' }],
		};
		(playlistsObject as any).PLAYLISTS = [testPlaylist];
		const { getByTestId } = render(<PlaylistInfoPage />);
		const genreEl = getByTestId('genre');
		expect(genreEl.innerHTML).toBe(testPlaylist.genre);
		const nameEl = getByTestId('name');
		expect(nameEl.innerHTML).toBe(testPlaylist.name);
		const tracksEl = getByTestId('tracks');
		expect(tracksEl.children.length).toBe(testPlaylist.tracks.length);
	});
});
