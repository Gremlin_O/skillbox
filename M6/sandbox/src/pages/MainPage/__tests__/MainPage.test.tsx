import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MainPage } from '../MainPage';

describe('Тест компонента MainPage', () => {
	test('Проверка снапшота', () => {
		jest.mock('react-router-dom', () => ({
			...jest.requireActual('react-router-dom'),
			useSearchParams: () => [new URLSearchParams({ ids: '001,002' })],
		}));
		const { container } = render(<MainPage />);
		expect(container).toMatchSnapshot();
	});
});
