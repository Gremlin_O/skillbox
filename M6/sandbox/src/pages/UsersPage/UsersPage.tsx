import { ChangeEvent, useState } from 'react';
import { USERS } from '../../data';
import './UsersPage.css';
import { Link, useSearchParams } from 'react-router-dom';

export function UsersPage() {
	const [searchParams, setSearchParams] = useSearchParams();

	const handleSearchName = (event: ChangeEvent<HTMLInputElement>): void => {
		setSearchParams({ search: event.target.value.toLowerCase() });
	};
	const search = searchParams.get('search') ?? '';
	const filteredUsers = USERS.filter(({ fullName }) =>
		fullName.toLowerCase().includes(search.toLowerCase())
	);

	return (
		<div className='usersPage'>
			<h2>UsersPage</h2>

			<div className='users'>
				<label>
					введите имя{' '}
					<input
						type='text'
						data-testid='search-input'
						value={search}
						onChange={handleSearchName}
					/>
				</label>

				{filteredUsers.map(({ id, fullName }) => (
					<Link to={`/users/${id}`} key={id}>
						{fullName}
					</Link>
				))}
			</div>
		</div>
	);
}
