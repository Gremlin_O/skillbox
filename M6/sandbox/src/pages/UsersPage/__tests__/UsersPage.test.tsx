import { fireEvent, render } from '@testing-library/react';
import { LinkProps, useSearchParams } from 'react-router-dom';
import { UsersPage } from '../UsersPage';

jest.mock('react-router-dom', () => ({
	...jest.requireActual('react-router-dom'),
	Link: (props: LinkProps) => <a></a>,
	useSearchParams: jest.fn(),
}));

describe('Тестирование компонента UsersPage', () => {
	test('setSearchParam вызывается при вводе имени пользователя', () => {
		const spy = jest.fn();
		(useSearchParams as jest.Mock).mockImplementation(() => [{ get: () => '' }, spy]);
		const { getByTestId } = render(<UsersPage />);
		const searchInput = getByTestId('search-input');
		fireEvent.change(searchInput, { target: { value: 'test' } });
		expect(spy).toHaveBeenCalled();
	});
});
