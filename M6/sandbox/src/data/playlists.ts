import { IPlaylist } from './interfaces';

export const PLAYLISTS: IPlaylist[] = [
	{
		id: 0,
		genre: 'Rock',
		name: 'Great Rock Hits',
		tracks: [
			{
				name: 'Tuti fruti',
			},
			{
				name: 'Lets dance',
			},

			{
				name: 'Oh',
			},

			{
				name: 'The Glow-Worm',
			},
		],
	},
	{
		id: 1,
		genre: 'Metal',
		name: 'Great music',
		tracks: [
			{
				name: 'Oh',
			},

			{
				name: 'The Glow-Worm',
			},
		],
	},
	{
		id: 2,
		genre: 'Non Music',
		name: 'Great Rock Hits2',
		tracks: [
			{
				name: 'Oh',
			},

			{
				name: 'The Glow-Worm',
			},
		],
	},
	{
		id: 3,
		genre: 'Classic',
		name: 'red sands',
		tracks: [
			{
				name: 'in the desert',
			},

			{
				name: 'first glance',
			},
		],
	},
];
