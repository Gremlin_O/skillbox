export interface IPlaylist {
	id: number;
	genre: string;
	name: string;
	tracks: {
		name: string;
	}[];
}

export interface IUser {
	id: number;
	email: string;
	fullName: string;
	jobTitle: string;
	avatar: string;
	bio: string;
	playlist?: IPlaylist;
}
