import PostsPage from '@/pages/PostsPage/PostsPage';
import TodoPage from '@/pages/TodoPage/TodoPage';

export default function Posts() {
	return <PostsPage />;
}
