const API_URL = 'http://localhost:3000';

export interface Restaurant {
	id: number;
	src: string;
	rating: number;
	country: string;
	title: string;
	ourRating: number;
}

export const getRestaurants = async (): Promise<Restaurant[]> => {
	const response = await fetch(`${API_URL}/restaurants`);
	return response.json();
};

export const reraitRestaurant = ({ id, rating }: { id: number; rating: number }) => {
	return fetch(`${API_URL}/restaurants/${id}`, {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			ourRating: rating,
		}),
	});
};
