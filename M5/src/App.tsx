import { useState } from 'react';
import reactLogo from './assets/react.svg';
import viteLogo from '/vite.svg';
import './App.css';
import { QueryClientProvider } from '@tanstack/react-query';
import BaseLayout from './ui/BaseLayout/BaseLayout';
import { queryClient } from './api/QueryClient';

function App() {
	const [count, setCount] = useState(0);

	return (
		<QueryClientProvider client={queryClient}>
			<BaseLayout />
		</QueryClientProvider>
	);
}

export default App;
