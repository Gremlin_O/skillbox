import { FC } from 'react';
import { useRestaurantsList } from '../../hooks/useRestaurantsList';
import style from './RestaurantCard.module.css';
import { Restaurant } from '../../api/restaraunts';
import Star from '/star.svg';

interface RestaurantCardProps {
	restaurant: Restaurant;
	ourRating: number | null;
	ourRatingChange: (rating: number) => void;
}

interface RaitingStarProps {
	marked: boolean;
	onClick: () => void;
}

const RaitingStar: FC<RaitingStarProps> = ({ marked, onClick }) => {
	return (
		<svg
			onClick={onClick}
			xmlns='http://www.w3.org/2000/svg'
			width='15'
			height='15'
			viewBox='0 0 24 24'
			fill={marked ? '#fc320f' : '#fefaf7'}
			stroke='#fc320f'
			cursor={'pointer'}
		>
			<path d='M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z' />
		</svg>
	);
};

const RestaurantCard: FC<RestaurantCardProps> = ({
	restaurant: { country, id, rating, src, title },
	ourRating,
	ourRatingChange,
}) => {
	return (
		<div className={style.card}>
			<div style={{ backgroundImage: `url(${src})` }} className={style.cardImg} />
			<h1 className={style.cardTitle}>{title}</h1>
			<h2 className={style.cardRating}>
				{country}, {rating} stars
			</h2>
			<div className={style.starCont}>
				{Array.from({ length: 5 }).map((e, index) => (
					<RaitingStar
						onClick={() => ourRatingChange(index + 1)}
						key={index}
						marked={ourRating ? index <= ourRating - 1 : false}
					/>
				))}
			</div>
		</div>
	);
};

export default RestaurantCard;
