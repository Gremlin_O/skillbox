import Avatar from '/public/avatar-1.jpg';
import style from './ProfileLogo.module.css';

const ProfileLogo = () => {
	return <img src={Avatar} alt='#' className={style.logo} />;
};

export default ProfileLogo;
