import SearchImg from '/search.svg';
import style from './Search.module.css';

interface SearchProps {
	text: string;
	onChange: (text: string) => void;
}

const Search = (props: SearchProps) => {
	return (
		<div className={style.search}>
			<img src={SearchImg} alt='#' className={style.searchImg} />
			<input
				type='text'
				placeholder='Search for restaurants'
				className={style.searchInput}
				value={props.text}
				onChange={(e) => props.onChange(e.target.value)}
			/>
		</div>
	);
};

export default Search;
