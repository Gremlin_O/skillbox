import { FC } from 'react';
import { queryClient } from '../../api/QueryClient';
import { useRate } from '../../hooks/useRateMutation';
import { useRestaurantsList } from '../../hooks/useRestaurantsList';
import RestaurantCard from '../RestaurantCard/RestaurantCard';
import style from './Restaurants.module.css';

interface RestaurantsProps {
	filter: string;
}

const Restaurants: FC<RestaurantsProps> = (props) => {
	const { data, isError, isLoading } = useRestaurantsList();
	const rateMutation = useRate();

	if (isLoading) {
		return (
			<div>
				<p className={style.loadingText}>
					Пожалуйста, подождите
					<br /> Загрузка ресторанов
				</p>
			</div>
		);
	}
	if (isError) {
		return (
			<div>
				<p className={style.errorText}>Произошла ошибка(</p>
				<button className={style.refetchButton}>Попробовать снова</button>
			</div>
		);
	}
	return (
		<div className={style.restaurantCont}>
			{data
				?.filter((item) => item.title.toLowerCase().indexOf(props.filter.toLowerCase()) > -1)
				.map((item) => (
					<RestaurantCard
						key={item.id}
						restaurant={item}
						ourRating={item.ourRating}
						ourRatingChange={(rating) => {
							rateMutation.mutateAsync({
								rating,
								id: item.id,
							});
						}}
					/>
				))}
		</div>
	);
};

export default Restaurants;
