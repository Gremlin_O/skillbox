import style from './BaseLayout.module.css';
import Logo from '/logo.svg';
import ProfileLogo from '../ProfileLogo/ProfileLogo';
import Search from '../Search/Search';
import Restaurants from '../Restaurants/Restaurants';
import { useState } from 'react';

const BaseLayout = () => {
	const [filterText, setFilterText] = useState('');

	return (
		<div className={style.mainWrapper}>
			<header className={style.header}>
				<div className={style.logoCont}>
					<img src={Logo} alt='#' className={style.logo} />
					<h1 className={style.logoHeader}>Eats</h1>
				</div>
				<ProfileLogo />
			</header>
			<main className={style.mainCont}>
				<Search text={filterText} onChange={(text) => setFilterText(text)} />
				<Restaurants filter={filterText} />
			</main>
			<footer className={style.footer}>
				<div className={style.footerTextCont}>
					<p className={style.footerText}>Privacy Policy</p>
					<p className={style.footerText}>Terms of Service</p>
				</div>
				<p className={style.footerText}>&#169; 2022 Eats</p>
			</footer>
		</div>
	);
};

export default BaseLayout;
