import { useQueries, useQuery } from '@tanstack/react-query';
import { Restaurant, getRestaurants } from '../api/restaraunts';

export const useRestaurantsList = () => {
	return useQuery({
		queryFn: getRestaurants,
		queryKey: ['restaurants'],
	});
};
