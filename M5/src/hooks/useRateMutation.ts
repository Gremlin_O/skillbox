import { useMutation } from '@tanstack/react-query';
import { Restaurant, getRestaurants, reraitRestaurant } from '../api/restaraunts';
import { queryClient } from '../api/QueryClient';

export const useRate = () => {
	return useMutation({
		mutationFn: reraitRestaurant,
		onSuccess: (data, updatedItem) => {
			const prevState = queryClient.getQueryData(['restaurants']) as Restaurant[];
			console.log(prevState);
			queryClient.setQueryData(
				['restaurants'],
				prevState.map((item: any) => ({
					...item,
					ourRating: item.id === updatedItem.id ? updatedItem.rating : item.ourRating,
				}))
			);
		},
		mutationKey: ['restaurants'],
	});
};
