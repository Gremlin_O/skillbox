import { useCallback, useEffect, useMemo, useState } from 'react';
import { fetchBestEmployeesData, fetchData, IData } from '../../api.ts';
import { BestEmployees } from '../BestEmployees/BestEmployees.tsx';
import { Team } from '../Team/Team.tsx';
import './styles.css';

function App() {
	const [month, setMonth] = useState(1);
	const [year, setYear] = useState(2024);
	const [data, setData] = useState<IData>();

	useEffect(() => {
		fetchData().then((data) => setData(data));
	}, []);

	const { profit = [], team = [] } = data || {};

	const profitSum = useMemo(() => profit.reduce((acc, i) => acc + i, 0), [profit]);
	const calculateProfitSum = useCallback((month: number) => month * profitSum, [profitSum]);

	const getApi = useCallback(() => fetchBestEmployeesData(year), [year]);

	const onIncMonth = useCallback(
		() => setMonth((count) => (count === 12 ? 1 : count + 1)),
		[setMonth]
	);
	const onDecYear = useCallback(
		() => setYear((count) => (count === 2018 ? 2018 : count - 1)),
		[setYear]
	);

	return (
		<div>
			<h1>App</h1>
			<div className='buttons'>
				<button onClick={onIncMonth}>month is {month}</button>
				<button onClick={onDecYear}>year is {year}</button>
			</div>
			<div className='block'>
				<p>
					Date is: {month}/{year}
				</p>
				<p>Profit sum is: {calculateProfitSum(month)} $</p>
			</div>
			<BestEmployees getApi={getApi} />
			<Team team={team} />
		</div>
	);
}

export default App;
