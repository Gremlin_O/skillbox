import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import './index.css';
import { Suspense } from 'react';
import { LoadingPage } from './pages/LoadingPage/LoadingPage.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
	<Suspense fallback={<LoadingPage />}>
		<App />
	</Suspense>
);
