import { lazy } from 'react';

export const AlbumsPageLazy = lazy(() => import('./AlbumsPage'));
