import React from 'react';

export const LoadingPage = () => {
	return <div style={{ margin: 'auto', fontSize: '50px' }}>Загрузка...</div>;
};
