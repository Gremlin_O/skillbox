import { Routes, Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import CusomLayout from './components/Layout/Layout';
import { HomePageLazy } from './pages/HomePage/HomePageLazy';
import { PostsPageLazy } from './pages/PostsPage/PostsPageLazy';
import { AlbumsPageLazy } from './pages/AlbumsPage/AlbumsPageLazy';
import { TodoPageLazy } from './pages/TodoPage/TodoPageLazy';

const App = () => {
	return (
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<CusomLayout />}>
					<Route index path='' element={<HomePageLazy />} />
					<Route path='posts' element={<PostsPageLazy />} />
					<Route path='albums' element={<AlbumsPageLazy />} />
					<Route path='todos' element={<TodoPageLazy />} />
				</Route>
			</Routes>
		</BrowserRouter>
	);
};

export default App;
